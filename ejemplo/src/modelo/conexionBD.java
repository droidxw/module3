/**
 * 
 */
package modelo;
//import java.sql.*;
import java.sql.Connection;
import java.sql.Statement;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author Java
 *
 */
public class conexionBD {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Connection conexion=null;
		
		String sentencia=null;
		//objeto para gestionar consultas hacia la B.D
		ResultSet resultado=null;
		
		//conexion a base de datos no a la sesion
		try {
			

			conexion=DriverManager.getConnection("jdbc:mysql://localhost:3306/dj_m3_almacen_computadoras", "root", "java");
			Statement sql=conexion.createStatement();
			
			
			//////////////CREACIÓN DE  TABLA
//			sql.executeUpdate("CREATE TABLE prueba(id_prueba SMALLINT NOT NULL PRIMARY KEY, prue_nombre VARCHAR(50) NOT NULL);");
			
			sentencia="CREATE TABLE prueba("
					+ "id_prueba SMALLINT NOT NULL PRIMARY KEY, "
					+ "prue_nombre VARCHAR(50) NOT NULL"
					+ ");";

			//////ADICIÓN DE TABLA
//			sql.executeUpdate(sentencia);
			
			
			//////ELIMINACION DE TABLA
//			sql.executeUpdate("DROP TABLE prueba;");
			System.out.println("...Sentencia SQL ejecutada");	
		
			//////CONSULTA EN TABLA

			resultado=sql.executeQuery("SELECT id_cliente, clien_telefono, clien_rfc  FROM cliente;");
//			System.out.println(resultado);
			
			/////FORMATO DE DATOS DE TABLA
			while (resultado.next()) {
				System.out.println("Matricula:"+resultado.getInt(1)+" Telefono:"+resultado.getString(2)+" RFC:"+resultado.getString(3));
				
			}
			//cierre de Statement
			sql.close();
			//cierre de conexion a B.D
			conexion.close();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			//traza complpeta
//			e.printStackTrace();
			System.out.println(e.getMessage());
		}
	}

}
